const express = require('express');
const app = express();
const port = 3000;

// All responses should be sent as JSON
// All responses should send an error respsonse if required query parameters are missing or invalid

app.get('/api/color/random', (req, res) => {
  // This endpoint should return a random color in HEX format
  // i.e. { "color": "#B4DA55" }
});

app.get('/api/color/to-rgb', (req, res) => {
  // This endpoint should receive a color via query string, for example `?color=B4DA55`
  // It should then return the color as an array of RGB values [RED, GREEN, BLUE],
  // i.e. { "color": [180, 218, 85] }
});

app.get('/api/color/to-hex', (req, res) => {
  // This endpoint should receive a color via query string in the format R-G-B, for example `?color=180-218-85`
  // It should then return the color as a hex string
  // i.e. { "color": "#B4DA55" }
});

app.get('/api/color/brightest', (req, res) => {
  // This endpoint should recieve two hex colors via a query string, for example `?color1=B4DA55&color2=FFFFFF`
  // It should then evaluate which color is brighter and return that color in hex format
  // i.e. { "color": "#FFFFFF" }
  // For our purposes, a color can be considered 'brighter' if the average of its RGB values is greater
});

app.get('/api/color/list', (req, res) => {
  // When this endpoint is called with the query param `color`, it should validate add that color to an
  // in-memory array of hex color values. For example, `?color=B4DA55`
  // When this endpoint is called with no query params, it should return the in-memory color list
  // For example { "colors": ["#FFFFFF", "#B4DA55"]}
  // The in-memory color list should never contain more than 5 items, new colors push old colors out
});

app.get('/api/color/dealers-choice', (req, res) => {
  // Make something up! This endpoint should accept some values and return JSON.
  // Stick with the theme of "color". 
});

app.listen(port, () => {
  console.log(`Test app listening at http://localhost:${port}`)
});

